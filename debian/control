Source: aeolus
Section: sound
Priority: optional
Maintainer: Debian Multimedia Maintainers <debian-multimedia@lists.debian.org>
Uploaders:
 Dennis Braun <d_braun@kabelmail.de>,
 Jaromír Mikeš <mira.mikes@seznam.cz>,
Build-Depends:
 debhelper-compat (= 13),
 libasound2-dev,
 libclthreads-dev,
 libclxclient-dev,
 libjack-jackd2-dev | libjack-dev,
 libreadline-dev,
 libzita-alsa-pcmi-dev,
 libfreetype6-dev,
 pkg-config
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/multimedia-team/aeolus.git
Vcs-Browser: https://salsa.debian.org/multimedia-team/aeolus
Homepage: https://kokkinizita.linuxaudio.org/linuxaudio/aeolus/
Rules-Requires-Root: no

Package: aeolus
Architecture: linux-any
Depends:
 stops,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 jackd,
Description: Synthesised pipe organ emulator
 Aeolus is a synthesised (i.e. not sampled) pipe organ emulator that
 should be good enough to make an organist enjoy playing it. It is a
 software synthesiser optimised for this job, with possibly hundreds
 of controls for each stop, that enable the user to "voice" his
 instrument.
 .
 Main features of the default instrument: three manuals and one pedal,
 five different temperaments, variable tuning, MIDI control of course,
 stereo, surround or Ambisonics output, flexible audio controls
 including a large church reverb.
 .
 Aeolus is not very CPU-hungry, and should run without problems on a
 e.g. a 1GHz, 256Mb machine.
